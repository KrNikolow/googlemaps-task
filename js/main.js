$(document).ready(function(){
 new formController().init();
});

var inputModel = function(){};

inputModel.prototype.getName = function(){
	return $('#name').val();
};

inputModel.prototype.setName = function(value){
	$('#name').val(value);
};

inputModel.prototype.getAddress = function(){
	return $('#address').val();
};

inputModel.prototype.setAddress = function(value){
	$('#address').val(value);
};

inputModel.prototype.getMail = function(){
	return $('#email').val();
};

inputModel.prototype.setMail = function(value){
	$('#email').val(value);
};

inputModel.prototype.getPhone = function(){
	return $('#phone').val();
};

inputModel.prototype.setPhone = function(value){
	$('#phone').val(value);
};

inputModel.prototype.getUrl = function(){
	return $('#url').val();
};

inputModel.prototype.setUrl = function(value){
	$('#url').val(value);
};

function myMap(mode) {
	var map = new google.maps.Map(document.getElementById('map'), {
    	zoom: 6,
    	center: {lat: 43.0846, lng: 25.6354}
	});

	new google.maps.places.Autocomplete(document.getElementById('address'));

	var geocoder = new google.maps.Geocoder();

	if(mode === 1){
		geocodeAddress(geocoder, map);
	}else if(mode === 2){
		submitMouse(geocoder, map);
	}
}
    
function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
	        resultsMap.setCenter(results[0].geometry.location);
	        var marker = new google.maps.Marker({
	        	map: resultsMap,
	            position: results[0].geometry.location,
	        });

	        google.maps.event.addListener(marker,'click',function() {
		   		resultsMap.setZoom(9);
		    	resultsMap.setCenter(marker.getPosition());
	  		});

	  		var infowindow = new google.maps.InfoWindow({
				content:results[0].formatted_address 
			});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(resultsMap,marker);
			});
        }
        else {
        	alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}


function submitMouse(geocoder, map){
	var marker;
	var infowindow = new google.maps.InfoWindow();
	google.maps.event.addListener(map, 'click', function(event) {
		
		placeMarker(map, event.latLng);
	});


	function placeMarker(map, location) {
		var lat = location.lat();
		var lng = location.lng();
		var latlng = new google.maps.LatLng(lat, lng);
		geocoder.geocode({'latLng': latlng}, function(result, status){
		  	if(status === 'OK'){
		  		if(result[0]){
		  			map.setZoom(9);
		  			 if (!marker){
				  		marker = new google.maps.Marker({
				    		position: location,
				   			map: map
			  			});
				  	}else{
				  		marker.setPosition(location);
				  	}

				  	if (!!infowindow && !!infowindow.close) {
					    infowindow.close();
					}

		  			infowindow = new google.maps.InfoWindow({
		    			content: result[0].formatted_address
		  			});
		  			infowindow.open(map,marker);
		  			document.getElementById('address').value = result[0].formatted_address;

					google.maps.event.addListener(marker,'click',function() {
		    			map.setZoom(9);
		    			map.setCenter(marker.getPosition());
		  			});
		  		}
			}
		});  
	}	
}

var formController = function(pModel){
 	this.model = pModel || new inputModel();
 	this.submitForm = function(){
		//Save inputs to localStorage
		localStorage.setItem("name", this.model.getName('name'));
     	localStorage.setItem("address", this.model.getAddress('address'));
     	localStorage.setItem("email", this.model.getMail('email'));     
     	localStorage.setItem("phone", this.model.getPhone('phone'));
     	localStorage.setItem("url", this.model.getUrl('url'));	
 	};

 	this.submitManually = function(){
 		//Set values from localStorage
		document.getElementById("name").value = localStorage.getItem("name");
   		document.getElementById("address").value = localStorage.getItem("address");
   		document.getElementById("email").value = localStorage.getItem("email");
   		document.getElementById("phone").value = localStorage.getItem("phone");
   		document.getElementById("url").value = localStorage.getItem("url");
 	};
};

formController.prototype.init = function(){
 	var self = this;	
 	
	var mode = 0;

	document.getElementById("inputs").addEventListener("submit", function(e){
		self.submitForm();
		mode = 1;
		myMap(mode);
		e.preventDefault();
	});

	$('#submitManually').click(function(){
		self.submitManually();
		mode = 2;
		myMap(mode);
	});
};

formController.prototype.getModel = function(){
    return this.model;
};